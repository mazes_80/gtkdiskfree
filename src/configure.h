/*  GtkDiskFree shows free space on your mounted partitions.  Copyright
    (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
    USA */

#ifndef GTKDISKFREE_CONFIGURE_H
#define GTKDISKFREE_CONFIGURE_H

#include "utils.h"

typedef struct
{
    /* Window pos/size */
    gint window_position[2];
    gint window_size[2];
    /* Blocks config */
    gint blocks[4];
    /* GUI config */
    gint sort[2];
    gint gui_style[4]; /* menubar, toolbar, statusbar, auto window resize*/
    gint column_style[4]; /* auto resize columns, Allow columns to
                                be resized, hide/show list titles,
                                rules hint */
    gint show_columns_all;
    gint show_columns[CAPACITY_COLUMN + 1];
    gint capacity_style[4]; /* gradients, percents, scaled, fixed
                                   size (if not scaled) */
    gdouble color1[3];
    gdouble color2[3];
    gdouble color3[3];
    gdouble color_text[3];
    gint popup[4]; /* type, show legend, interpolate snail */
    /* Update */
    gfloat update_interval;
    /* Mount commands */
    gchar *mount_cmds[2];
    GList *skip_fs;
    GList *mountpoint_force;
} prefs_t;

extern prefs_t *options;


prefs_t *cfg_new (void);
void cfg_init (gint, gchar **);
void cfgfil_read (prefs_t *);
void cfgfil_write (prefs_t *);
void cfgfil_default (prefs_t *);
void cfg_load_default (void);
void cfg_free (prefs_t *);
void cfg_cpy (prefs_t *, const prefs_t *);
#endif // GTKDISKFREE_CONFIGURE_H
