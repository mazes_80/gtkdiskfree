/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#include "interface.h"
#include "configure.h"
#include "dialog.h"
#include "diskfree.h"
#include "menus.h"
#include "pixmap.h"

#include "icons/icon.xpm"

/* Signals */
gint block_size_status;
gint timeout_status;

GtkWidget *mwindow;

GtkWidget *list_box, *list_scroll;

GtkWidget *status_hbox;
GtkWidget *status_bar_block;
GtkWidget *status_bar_timeout;

gint capacity_resize_timeout = 0;

GtkTreeModel *list_treemodel;
GtkWidget *list_treeview;

GtkWidget *menu_frame;
GtkWidget *tool_frame;

gboolean gui_color_update;

void gui_capacity_column_resize_end (gpointer data)
{
    capacity_resize_timeout = 0;

    return;
}

gint gui_list_get_capacities_width ()
{
    gint width;

    /* Get size of the capacity column. */
    if (options->capacity_style[2] && !options->gui_style[3]) {
        gint idx;
        GtkTreeViewColumn *column;
        gtk_window_get_size(GTK_WINDOW(mwindow), &width, NULL);
        for(idx=0; idx != CAPACITY_COLUMN; idx++) {
            column = gtk_tree_view_get_column(GTK_TREE_VIEW(list_treeview), idx);
            if(options->show_columns[idx] || options->show_columns_all)
                width -= gtk_tree_view_column_get_width(column);
        }
        width -= 6;

        /* Todo return 0 when */
        if(width < 64)
            width=64;
    } else
        width = options->capacity_style[3];

    return width;
}

void gui_list_capacities_init(GList *list)
{
    GList *idx = g_list_first(list);
    gint width = gui_list_get_capacities_width();
    gint real = 1;

    filesystem_t *fs;

    while(idx) {
        fs = FILESYSTEM(list->data);
        if (fs->need_update || gui_color_update) {
            /* Real isn't working for display, use a twist */
            if(fs->pused == 100 && fs->size == 0 && fs->pfree == 0)
                real=0;

            fs->pixbuf = pixmap_draw_capacity(real, fs->mounted, fs->pused, width);
        }
        idx = idx->next;
    }
}

void gui_list_capacity_redraw (GtkTreeIter *iter, gint width)
{
    gint percent, status;
    gint real, freep;
    GdkPixbuf *pixbuf = NULL;

    gtk_tree_model_get(list_treemodel, iter,
                        SIZEK_COLUMN, &real,
                        FREEP_PERCENT_COLUMN, &freep,
                        USEDP_PERCENT_COLUMN, &percent,
                        STATUS_COLUMN, &status,
                        CAPACITY_COLUMN, &pixbuf,
                        -1);

    if(pixbuf) {
        g_object_unref(pixbuf);
        pixbuf = NULL;
    }

    /* Real isn't working for display, use a twist */
    if(percent == 100 && real == 0 && freep == 0)
        real=0;

    pixbuf = pixmap_draw_capacity(real, status, percent, width);

    gtk_list_store_set(GTK_LIST_STORE(list_treemodel), iter,
                        CAPACITY_COLUMN, pixbuf,
                        -1);

    return;
}

void gui_show_selection(GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn  *col, gpointer data)
{
    GtkTreeViewColumn *test_col;
    GtkTreeIter iter;
    gint mounted;

    if(!gtk_tree_model_get_iter(list_treemodel, &iter, path))
        return;

    /* Feed with data from selection */
    gtk_tree_model_get (list_treemodel, &iter, STATUS_COLUMN, &mounted, -1);

    /* Identify column */
    test_col = gtk_tree_view_get_column(treeview, MOUNT_COLUMN);
    if ( test_col == col ) {
        gchar *mntpt, *type;

        gtk_tree_model_get(list_treemodel, &iter,
                            MOUNT_POINT_COLUMN, &mntpt,
                            FILESYSTEM_TYPE_COLUMN, &type,
                            -1);

        gboolean success = TRUE;
        gchar *error;
        error = (gchar *)g_malloc(sizeof(gchar) * MAXLINE);

        if(mounted == FS_MOUNTED)
            success = open_cmdtube(options->mount_cmds[1], mntpt, &error);
        else if(mounted == FS_UMOUNTED)
            success = open_cmdtube(options->mount_cmds[0], mntpt, &error);

        if(!success)
            error_dialog(error);

        g_free(error);

        gui_list_main_update(GTK_TREE_VIEW(list_treeview));

        //gui_list_capacity_redraw(&iter, gui_list_get_capacities_width());
        gui_color_update = TRUE;
        gui_list_capacities_redraw();

        return;
    }

    if(mounted != FS_MOUNTED)
        return;

    if(options->popup[0] != 0)
        widget_dialog_chart(list_treemodel, &iter);

    return;
}

/* gboolean gui_popup_signal (GtkWidget *widget, GdkEventButton *ev, gpointer data)
{
    if (ev->button == 3)
        return gui_popup_menu_signal(widget, ev, NULL);

    return FALSE;
} */

gboolean gui_list_capacities_redraw ()
{
    static gint last_width = 0;

    GtkTreeIter iter;
    gint width;

    /* If the capacity column has same width than the last time we
       update it, also we do nothing. Same thing if width <= 0. */
    width = gui_list_get_capacities_width();
    if (last_width == width && !gui_color_update)
        return FALSE;

    if (!gtk_tree_model_get_iter_first(list_treemodel, &iter))
        return FALSE;

    do { gui_list_capacity_redraw(&iter, width);
    } while (gtk_tree_model_iter_next(list_treemodel, &iter));

    last_width = width;
    gui_color_update = FALSE;

    /* Todo: Maybe not here */
    if (options->column_style[1])
        gtk_tree_view_columns_autosize(GTK_TREE_VIEW(list_treeview));

    return FALSE;
}

gboolean gui_capacity_column_resize_start (GtkTreeViewColumn *cl, gpointer data)
{
    if (capacity_resize_timeout)
        g_source_remove(capacity_resize_timeout);
    capacity_resize_timeout = g_timeout_add_full(G_PRIORITY_DEFAULT,
                                125,
                                (GSourceFunc)gui_list_capacities_redraw,
                                NULL,
                                (GDestroyNotify)gui_capacity_column_resize_end);

    return FALSE;
}

gint gui_list_column_get_position (GtkTreeViewColumn *column)
{
    gint i = 0;
    GList *columns_list = NULL;

    columns_list = gtk_tree_view_get_columns(GTK_TREE_VIEW(list_treeview));

    while (GTK_TREE_VIEW_COLUMN(columns_list->data) != column && i <= CAPACITY_COLUMN) {
        columns_list = columns_list->next;
        i++;
    }
    columns_list = g_list_first(columns_list);
    g_list_free(columns_list);

    return i;
}

void gui_list_column_clicked (GtkTreeViewColumn *column, gpointer user_data)
{
    options->sort[0] = gui_list_column_get_position(column);
    options->sort[1] = gtk_tree_view_column_get_sort_order(column);

    return;
}

void gui_list_columns_disp_update (void)
{
    gint i = 0;
    GList *it = NULL;
    GList *columns_list = NULL;

    columns_list = gtk_tree_view_get_columns(GTK_TREE_VIEW(list_treeview));
    columns_list = g_list_first(columns_list);
    it = columns_list;

    while (it != NULL && i < N_COLUMNS) {
        if (i > CAPACITY_COLUMN)
            gtk_tree_view_column_set_visible(GTK_TREE_VIEW_COLUMN(it->data),
                            FALSE);
        else {
            if (options->show_columns[i] || options->show_columns_all)
                gtk_tree_view_column_set_visible(GTK_TREE_VIEW_COLUMN(it->data),
                                TRUE);
            else
                gtk_tree_view_column_set_visible(GTK_TREE_VIEW_COLUMN(it->data),
                                FALSE);
            if (options->column_style[0])
                gtk_tree_view_column_set_sizing(GTK_TREE_VIEW_COLUMN(it->data),
                                GTK_TREE_VIEW_COLUMN_AUTOSIZE);
            else
                gtk_tree_view_column_set_sizing(GTK_TREE_VIEW_COLUMN(it->data),
                                GTK_TREE_VIEW_COLUMN_FIXED);
            if (options->column_style[1])
                gtk_tree_view_column_set_resizable(GTK_TREE_VIEW_COLUMN(it->data),
                                    TRUE);
            else
                gtk_tree_view_column_set_resizable(GTK_TREE_VIEW_COLUMN(it->data),
                                    FALSE);
        }

        it = it->next;
        i++;
    }
    g_list_free(columns_list);

    if (options->column_style[2])
        gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(list_treeview), TRUE);
    else
        gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(list_treeview), FALSE);

#if GTK_CHECK_VERSION(3, 0, 0)
    // Theme dependant, no theme implements it ???
#else
    if (options->column_style[3])
        gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(list_treeview), TRUE);
    else
        gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(list_treeview), FALSE);
#endif

    return;
}

void gui_list_columns_init_sortway (void)
{
    GtkTreeViewColumn *column;

    column = gtk_tree_view_get_column(GTK_TREE_VIEW(list_treeview), options->sort[0]);
    gtk_tree_view_column_set_sort_indicator(column, TRUE);
    if (options->sort[1] == GTK_SORT_ASCENDING)
        gtk_tree_view_column_set_sort_order(column, GTK_SORT_ASCENDING);
    else
        gtk_tree_view_column_set_sort_order(column, GTK_SORT_DESCENDING);
    gtk_tree_view_column_clicked(column);

    return;
}

void gui_statusbar_timeout_update (GtkWidget *widget, order_t action)
{
    /* timeout is used by GTK+ to remember which function and when
            to call it, GTK+ use only > 0 values, here we use -1 value
            to know if there is a timeout. */
    static gint timeout = -1;

    switch (action) {
    case UPDATE:
        if (timeout < 0) {
            gui_list_main_update(GTK_TREE_VIEW(list_treeview));
            break;
        }
    case REFRESH:
        if (timeout > 0)
            g_source_remove(timeout);
        gui_list_main_update(GTK_TREE_VIEW(list_treeview));
        timeout = g_timeout_add((gint) (1000 * options->update_interval),
                        (GSourceFunc)gui_list_main_update,
                        (gpointer)list_treeview);
        gtk_statusbar_pop(GTK_STATUSBAR(status_bar_timeout), timeout_status);
        break;
    case STOP:
        if (timeout > 0)
            g_source_remove(timeout);
        gtk_statusbar_pop(GTK_STATUSBAR(status_bar_timeout), timeout_status);
        gtk_statusbar_push(GTK_STATUSBAR(status_bar_timeout), timeout_status,
                    _(" Stopped..."));
        timeout = -1;
        break;
    default:
        break;
    }

    return;
}

gboolean gui_statusbar_update_block (void)
{
    gtk_statusbar_pop(GTK_STATUSBAR(status_bar_block), block_size_status);

    switch (options->blocks[0]) {
    case GIGABYTE:
        gtk_statusbar_push(GTK_STATUSBAR(status_bar_block),
                    block_size_status, _(" Gigabytes"));
        break;
    case MEGABYTE:
        gtk_statusbar_push(GTK_STATUSBAR(status_bar_block),
                    block_size_status, _(" Megabytes"));
        break;
    case KILOBYTE:
        gtk_statusbar_push(GTK_STATUSBAR(status_bar_block),
                    block_size_status, _(" Kilobytes"));
        break;
    default:
        options->blocks[0] = AUTOSIZE;
    case AUTOSIZE:
        gtk_statusbar_push(GTK_STATUSBAR(status_bar_block),
                    block_size_status, _(" Human readable"));
        break;
    }

    return TRUE;
}

void gui_main_window_set_posize (void)
{
    GdkRectangle workarea = {0};
    gdk_monitor_get_workarea(
                gdk_display_get_primary_monitor(gdk_display_get_default()),
                    &workarea);
    if (workarea.width >= options->window_position[0] &&
        workarea.height >= options->window_position[1] &&
        (options->window_position[0] != -1 && options->window_position[1] != -1)) {
        gtk_window_move(GTK_WINDOW(mwindow),
                options->window_position[0],
                options->window_position[1]);
        gtk_window_resize(GTK_WINDOW(mwindow),
                    options->window_size[0],
                    options->window_size[1]);
    }

    return;
}

void gui_main_window_disp_update (void)
{
    if (options->gui_style[0])
        gtk_widget_show(menu_frame);
    else
        gtk_widget_hide(menu_frame);

    if (options->gui_style[1])
        gtk_widget_show(tool_frame);
    else
        gtk_widget_hide(tool_frame);

    if (options->gui_style[2])
        gtk_widget_show(status_hbox);
    else
        gtk_widget_hide(status_hbox);

    return;
}

void gui_main_window_resize (void)
{
    if (!gtk_widget_get_visible(mwindow))
        return;

    if (options->gui_style[3]) {
        gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(list_scroll),
                            GTK_POLICY_NEVER,
                            GTK_POLICY_NEVER);
        gtk_window_set_decorated(GTK_WINDOW(mwindow), TRUE);
        gtk_window_set_resizable(GTK_WINDOW(mwindow), FALSE);
    } else {
        gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(list_scroll),
                            GTK_POLICY_AUTOMATIC,
                            GTK_POLICY_AUTOMATIC);
        gtk_window_set_resizable(GTK_WINDOW(mwindow), TRUE);
    }

    return;
}

gboolean gui_main_window_configure (GtkWidget *win, GdkEventConfigure *ev,
                gpointer data)
{
    GdkRectangle workarea = {0};
    gint w, h, ret = FALSE;

    gdk_monitor_get_workarea(
                gdk_display_get_primary_monitor(gdk_display_get_default()),
                    &workarea);
    w = workarea.width;
    h = workarea.height;
    options->window_position[0] = ev->x;
    options->window_position[1] = ev->y;

    if (ev->width <= w)
        options->window_size[0] = ev->width;
    else {
        options->window_size[0] = w;
        ret = TRUE;
    }
    if (ev->height <= h)
        options->window_size[1] = ev->height;
    else {
        options->window_size[1] = h;
        ret = TRUE;
    }

    if (ret)
        gtk_window_resize(GTK_WINDOW(mwindow),
                    options->window_size[0],
                    options->window_size[1]);

    return FALSE;
}

void gui_list_columns_create (GtkTreeView *list)
{
    gint i;
    gchar *titles[CAPACITY_COLUMN + 1] = {_("Filesystem"), _("Size"), _("Used"),
                            _("Free"), _("Used %"), _("Free %"),
                            _("Type"), _("Options"),
                            _("Mountpoint"), "", _("Capacity")};
    GtkCellRenderer *cell_renderer;
    GtkTreeViewColumn *column;

    /* We want to display text in our list, so cell will be
            text. */
    for (i = 0 ; i < MOUNT_COLUMN ; i++) {
        cell_renderer = gtk_cell_renderer_text_new();
        column = gtk_tree_view_column_new_with_attributes(titles[i], cell_renderer,
                                    "text", i, NULL);
        g_signal_connect(G_OBJECT(column), "clicked",
                G_CALLBACK(gui_list_column_clicked), NULL);
        switch (i) {
            case SIZE_COLUMN:
                gtk_tree_view_column_set_sort_column_id(column, SIZEK_COLUMN);
                break;
            case USED_SPACE_COLUMN:
                gtk_tree_view_column_set_sort_column_id(column, USEDK_SPACE_COLUMN);
                break;
            case FREE_SPACE_COLUMN:
                gtk_tree_view_column_set_sort_column_id(column, FREEK_SPACE_COLUMN);
                break;
            case USED_PERCENT_COLUMN:
                gtk_tree_view_column_set_sort_column_id(column, USEDP_PERCENT_COLUMN);
                break;
            case FREE_PERCENT_COLUMN:
                gtk_tree_view_column_set_sort_column_id(column, FREEP_PERCENT_COLUMN);
                break;
            default:
                gtk_tree_view_column_set_sort_column_id(column, i);
                break;
        }
        gtk_tree_view_append_column(list, column);
    }

    /* Except last column which is a 'pixmap' or 'pixbuf'
        column. */
    cell_renderer = gtk_cell_renderer_pixbuf_new();
    g_object_set(G_OBJECT(cell_renderer), "xalign", 0.0, NULL);
    column = gtk_tree_view_column_new_with_attributes(titles[i], cell_renderer,
                                "pixbuf", i, NULL);
    gtk_tree_view_column_set_sort_column_id(column, USEDP_PERCENT_COLUMN);
    gtk_tree_view_append_column(list, column);

    g_signal_connect(G_OBJECT(column), "clicked",
            G_CALLBACK(gui_list_column_clicked), NULL);

    g_object_set(G_OBJECT(cell_renderer), "xalign", 0.0, NULL);
    column = gtk_tree_view_column_new_with_attributes(titles[i+1], cell_renderer,
                                "pixbuf", i+1, NULL);
    gtk_tree_view_column_set_sort_column_id(column, USEDP_PERCENT_COLUMN);
    gtk_tree_view_append_column(list, column);

    g_signal_connect(G_OBJECT(column), "clicked",
            G_CALLBACK(gui_list_column_clicked), NULL);
    g_signal_connect(G_OBJECT(column), "notify::width",
            G_CALLBACK(gui_capacity_column_resize_start), NULL);

    return;
}

/* Define the treeview wigdet we will use later in this apps */
GtkTreeModel *gui_list_model_create (void)
{
    GtkListStore *store = gtk_list_store_new(N_COLUMNS,
                        G_TYPE_STRING, // File
                        G_TYPE_STRING, // Size
                        G_TYPE_STRING, // Used space
                        G_TYPE_STRING, // Free space
                        G_TYPE_STRING, // Used percent
                        G_TYPE_STRING, // Free percent total
                        G_TYPE_STRING, // FS type
                        G_TYPE_STRING, // Mount opts
                        G_TYPE_STRING, // Mount point
                        GDK_TYPE_PIXBUF, // Mount/umount clickable
                        GDK_TYPE_PIXBUF, // Capacity

                        G_TYPE_ULONG,   // Size in Kb (used to sort)
                        G_TYPE_ULONG,   // Used space in Kb (used to sort)
                        G_TYPE_ULONG,   // Free space in Kb (used to sort)
                        G_TYPE_INT,    // Used percent
                        G_TYPE_INT,    // Free percent
                        G_TYPE_INT    // Status
            );

    return GTK_TREE_MODEL(store);
}

/* creates & shows the main window */
void gui_main_window_create (void)
{
    GtkWidget *main_box, *menu_box;
    GdkPixbuf *icon;

    GtkTreeSelection *selection;

    GtkWidget *status_table, *status_bar;

    /*--MainWindow--*/
    mwindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(mwindow), "gtkdiskfree");
    g_signal_connect(G_OBJECT(mwindow), "destroy",
            G_CALLBACK(gtk_main_quit), NULL);
    gtk_container_set_border_width(GTK_CONTAINER(mwindow), 0);
    gtk_widget_realize(mwindow);

    icon = gdk_pixbuf_new_from_xpm_data(icon_xpm);

    gtk_window_set_icon_name(GTK_WINDOW(mwindow), "GtkDiskFree");
    gtk_window_set_icon(GTK_WINDOW(mwindow), icon);
    gtk_window_set_default_icon(icon);

    GTK_BOX_V(main_box, FALSE, 1)
    gtk_container_add(GTK_CONTAINER(mwindow), main_box);
    gtk_widget_show(main_box);

    /*--MenuBar & ToolBar--*/
    GTK_BOX_V(menu_box, FALSE, 1)
    gtk_box_pack_start(GTK_BOX(main_box), menu_box, FALSE, FALSE, 0);
    gtk_widget_show(menu_box);

    gui_main_menu_create(menu_box);
    gui_toolbar_create(menu_box);

    /*--List--*/
    GTK_BOX_V(list_box, FALSE, 1)
    gtk_box_pack_start(GTK_BOX(main_box), list_box, TRUE, TRUE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(list_box), 0);
    gtk_widget_show(list_box);

    list_scroll = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(list_scroll),
                        GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_box_pack_start(GTK_BOX(list_box), list_scroll, TRUE, TRUE, 0);
    gtk_widget_show(list_scroll);

    /* We get the model from the function we did before. */
    list_treemodel = gui_list_model_create();

    /* We create the widget from the model. */
    list_treeview = gtk_tree_view_new_with_model(list_treemodel);
    g_object_unref(G_OBJECT(list_treemodel));

    /* GtkTreeView config */
    selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(list_treeview));
    gtk_tree_selection_set_mode(selection, GTK_SELECTION_SINGLE);

    gtk_tree_view_set_reorderable(GTK_TREE_VIEW(list_treeview), FALSE);

    gtk_container_add(GTK_CONTAINER(list_scroll), list_treeview);

    /* We need to create & defined cells (title and others). */
    gui_list_columns_create(GTK_TREE_VIEW(list_treeview));

    /* Signals */
    /* g_signal_connect(G_OBJECT(list_treeview), "button-press-event",
            G_CALLBACK(gui_popup_signal), NULL); */
    g_signal_connect(G_OBJECT(list_treeview), "row-activated",
            G_CALLBACK(gui_show_selection), NULL);
    gtk_widget_show_all(list_treeview);

    /*--StatusBar--*/
    GTK_BOX_H(status_hbox, FALSE, 1)
    gtk_box_pack_start(GTK_BOX(main_box), status_hbox, FALSE, FALSE, 0);
    gtk_widget_show(status_hbox);

#if GTK_CHECK_VERSION(3, 0, 0)
    status_table = gtk_grid_new();
    gtk_grid_set_column_homogeneous(GTK_GRID(status_table), TRUE);
#else
    status_table = gtk_table_new(1, 5, TRUE);
#endif
    gtk_box_pack_start(GTK_BOX(status_hbox), status_table, TRUE, TRUE, 0);
    gtk_widget_show(status_table);

    status_bar = gtk_statusbar_new();
#if GTK_CHECK_VERSION(3, 0, 0)
    gtk_grid_attach(GTK_GRID(status_table), status_bar, 0, 0, 1, 1);
#else
    gtk_statusbar_set_has_resize_grip(GTK_STATUSBAR(status_bar), FALSE);
    gtk_table_attach(GTK_TABLE(status_table), status_bar, 0, 3, 0, 1,
            GTK_FILL | GTK_EXPAND, GTK_FILL, 0, 0);
#endif
    gtk_widget_show(status_bar);

    status_bar_timeout = gtk_statusbar_new();
#if GTK_CHECK_VERSION(3, 0, 0)
    gtk_grid_attach(GTK_GRID(status_table), status_bar_timeout, 1, 0, 1, 1);
#else
    gtk_statusbar_set_has_resize_grip(GTK_STATUSBAR(status_bar_timeout), FALSE);
    gtk_table_attach(GTK_TABLE(status_table), status_bar_timeout, 3, 4, 0, 1,
            GTK_FILL | GTK_EXPAND, GTK_FILL, 0, 0);
#endif
    gtk_widget_show(status_bar_timeout);
    timeout_status = gtk_statusbar_get_context_id(GTK_STATUSBAR(status_bar_timeout),
                                "Program status");
    status_bar_block = gtk_statusbar_new();
#if GTK_CHECK_VERSION(3, 0, 0)
    gtk_grid_attach(GTK_GRID(status_table), status_bar_block, 2, 0, 1, 1);
#else
    gtk_statusbar_set_has_resize_grip(GTK_STATUSBAR(status_bar_block), FALSE);
    gtk_table_attach(GTK_TABLE(status_table), status_bar_block, 4, 5, 0, 1,
            GTK_FILL | GTK_EXPAND, GTK_FILL, 0, 0);
#endif
    gtk_widget_show(status_bar_block);
    block_size_status = gtk_statusbar_get_context_id(GTK_STATUSBAR(status_bar_block),
                            "Blocks size status");
    gtk_widget_show(status_hbox);
    gui_statusbar_update_block();

    /*-------*/
    gtk_widget_show(mwindow);

    gui_main_window_disp_update();
    gui_list_columns_disp_update();
    gui_list_columns_init_sortway();

    gui_main_window_set_posize();
    gui_main_window_resize();

    g_signal_connect(G_OBJECT(mwindow), "configure-event",
            G_CALLBACK(gui_main_window_configure), NULL);

    gtk_widget_show(mwindow);

    /* GtkTreePath *path = gtk_tree_path_new_first();
    gtk_tree_view_set_cursor(GTK_TREE_VIEW(list_treeview), path, NULL, FALSE);
    gtk_tree_path_free(path); */

    return;
}
