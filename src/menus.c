/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#include "menus.h"
#include "configure.h"
#include "dialog.h"
#include "interface.h"
#include "options.h"
#include "widgets.h"

#define SPACE_CHAR " "

GtkWidget *selection_widget;
GtkToolItem *toggle;
GtkToolItem *refresh;
// GtkWidget *popup;

/* Signals */
/* void gui_menu_copy_clicked (GtkWidget *widget, gint *have_selection)
{
    *have_selection = gtk_selection_owner_set(selection_widget,
                            GDK_SELECTION_PRIMARY,
                            GDK_CURRENT_TIME);
    return;
} */

/* void gui_menu_copy_handle (GtkWidget *widget, GtkSelectionData *selection_data,
                guint info, guint time_stamp, gint *have_selection)
{
    gint i;
    gchar *str = NULL, *s1, *s2;
    GtkTreeIter iter;
    GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(list_treeview));

    if (!gtk_tree_selection_get_selected(selection, NULL, &iter))
        return;

    for (i = FILESYSTEM_COLUMN ; i <= MOUNT_POINT_COLUMN ; i++) {
        if (options->show_columns[i] || options->show_columns_all) {
            gtk_tree_model_get(list_treemodel, &iter, i, &s2, -1);
            if (str != NULL) {
                s1 = g_strdup(str);
                g_free(str);
                str = g_strconcat(s1, SPACE_CHAR, SPACE_CHAR, s2, NULL);
                g_free(s1);
            } else
                str = g_strdup(s2);
            g_free(s2);
        }
    }
    gtk_selection_data_set_text(selection_data, str, strlen(str));
    g_free(str);

    return;
}*/

/* gboolean gui_menu_copy_clear (GtkWidget *widget, GdkEventSelection *event,
                gint *have_selection)
{
    *have_selection = FALSE;

    return TRUE;
} */

/* gboolean gui_popup_menu_signal (GtkWidget *widget, GdkEventButton *ev,
                gpointer data)
{
    if (gtk_widget_get_visible(popup))
        gtk_menu_popdown(GTK_MENU(popup));
    else
        gtk_menu_popup(GTK_MENU(popup), NULL,
                    NULL, NULL, NULL,
                    ev->button, ev->time);
    return TRUE;
} */

/* Create */
void gui_main_menu_create (GtkWidget *box)
{
    GtkWidget *menushell;
    GtkWidget *main_menu;
    GtkWidget *main_item;
    GtkWidget *menu_item;

    /* -- Create menu bar -- */
    menu_frame = gtk_frame_new( NULL );
    gtk_box_pack_start(GTK_BOX(box), menu_frame, TRUE, FALSE, 0);

    menushell = gtk_menu_bar_new();
    gtk_container_add(GTK_CONTAINER(menu_frame), menushell);
    gtk_widget_show(menushell);

    /* -- File menu -- */
    main_item = gtk_menu_item_new_with_label(_("File"));
    gtk_menu_shell_append(GTK_MENU_SHELL(menushell), main_item);
    gtk_widget_show(main_item);

    main_menu = widget_menu_add(main_item);

    menu_item = widget_menu_item_add(main_menu, _("Properties"));
    gtk_widget_show(menu_item);
    g_signal_connect(G_OBJECT(menu_item), "activate",
                G_CALLBACK(ui_options_window_create),
                NULL);

    widget_menu_item_add(main_menu, NULL);

    menu_item = widget_menu_item_add(main_menu, _("Exit"));
    g_signal_connect_object(G_OBJECT(menu_item), "activate",
                G_CALLBACK(gtk_main_quit), NULL, 0);

    /* -- Help menu -- */
#if GTK_CHECK_VERSION(3, 0, 0)
    main_item = gtk_menu_item_new();
    gtk_widget_set_hexpand (GTK_WIDGET(main_item), TRUE);
    gtk_widget_set_halign (GTK_WIDGET(main_item), GTK_ALIGN_FILL);
    gtk_menu_shell_append( GTK_MENU_SHELL(menushell), main_item);

    main_item = gtk_menu_item_new_with_label(_("Help"));
#else
    main_item = gtk_menu_item_new_with_label(_("Help"));
    gtk_menu_item_set_right_justified(GTK_MENU_ITEM(main_item), TRUE);
#endif
    gtk_menu_shell_append( GTK_MENU_SHELL(menushell), main_item);
    gtk_widget_show(main_item);

    main_menu = widget_menu_add(main_item);

    menu_item = widget_menu_item_add(main_menu, _("About"));
    g_signal_connect(G_OBJECT(menu_item), "activate",
                G_CALLBACK(gui_about_window), NULL);

    gtk_widget_show_all(menu_frame);

    return;
}

void toolbar_toggle_refresh_button(GtkWidget *widget, order_t action)
{
    static gboolean refresh_data = TRUE;
    static GtkWidget *icon1 = NULL;
    static GtkWidget *icon2 = NULL;

    if(icon2 == NULL) {
        icon1 = gtk_tool_button_get_icon_widget(GTK_TOOL_BUTTON(toggle));
        icon2 = gtk_image_new_from_icon_name ("media-playback-stop", GTK_ICON_SIZE_LARGE_TOOLBAR);
        g_object_ref_sink(G_OBJECT(icon1));
        g_object_ref_sink(G_OBJECT(icon2));
    }

    if(G_TYPE_FROM_INSTANCE(widget) == GTK_TYPE_TOOL_BUTTON) {
        if(refresh_data)
            action = STOP;
        else
            action = REFRESH;
    }

    if(action == REFRESH) {
        gtk_tool_button_set_icon_widget(GTK_TOOL_BUTTON(toggle), icon1);
        gtk_widget_hide(GTK_WIDGET(refresh));
    }
    else if(action == STOP) {
        gtk_tool_button_set_icon_widget(GTK_TOOL_BUTTON(toggle), icon2);
        gtk_widget_show(GTK_WIDGET(refresh));
    }

    gui_statusbar_timeout_update(GTK_WIDGET(widget), action);
    gtk_widget_show_all(GTK_WIDGET(toggle));
    refresh_data = ! refresh_data;
}

void gui_toolbar_create (GtkWidget *box)
{
    GtkToolItem *toolbutton;
    GtkWidget *toolbar;
    GtkWidget *icon;

    tool_frame = gtk_frame_new( NULL );

    gtk_box_pack_start(GTK_BOX(box), tool_frame, FALSE, FALSE, 0);

    toolbar = gtk_toolbar_new();
    gtk_orientable_set_orientation(GTK_ORIENTABLE(toolbar), GTK_ORIENTATION_HORIZONTAL);
    gtk_container_add(GTK_CONTAINER(tool_frame), toolbar);
    gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_ICONS);
    gtk_toolbar_set_icon_size(GTK_TOOLBAR(toolbar), GTK_ICON_SIZE_DND);
    gtk_widget_show_all(tool_frame);

    icon = gtk_image_new_from_icon_name ("media-playback-start", GTK_ICON_SIZE_LARGE_TOOLBAR);
    toggle = gtk_tool_button_new(icon, _("Toggle refresh"));
    gtk_tool_item_set_tooltip_text(GTK_TOOL_ITEM(toggle), _("Toggle refresh"));
    g_signal_connect(G_OBJECT(toggle), "clicked",
                G_CALLBACK(toolbar_toggle_refresh_button),
                (gpointer)REFRESH);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), toggle, -1);

    icon = gtk_image_new_from_icon_name ("view-refresh", GTK_ICON_SIZE_LARGE_TOOLBAR);
    refresh = gtk_tool_button_new(icon, _("Refresh"));
    gtk_tool_item_set_tooltip_text(GTK_TOOL_ITEM(refresh), _("Update once"));
    g_signal_connect(G_OBJECT(refresh), "clicked",
                G_CALLBACK(gui_statusbar_timeout_update),
                (gpointer)UPDATE);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), refresh, -1);

    /* Fake entry -> spacer */
    toolbutton = gtk_tool_button_new(NULL, NULL);
    gtk_widget_set_sensitive(GTK_WIDGET(toolbutton), FALSE);
    gtk_tool_item_set_expand (GTK_TOOL_ITEM(toolbutton), TRUE);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), toolbutton, -1);


    icon = gtk_image_new_from_icon_name ("preferences-desktop", GTK_ICON_SIZE_LARGE_TOOLBAR);
    toolbutton = gtk_tool_button_new(icon, _("Properties"));
    gtk_tool_item_set_tooltip_text(GTK_TOOL_ITEM(toolbutton), _("Preference dialog"));
    g_signal_connect(G_OBJECT(toolbutton), "clicked",
                G_CALLBACK(ui_options_window_create),
                NULL);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), toolbutton, -1);

    icon = gtk_image_new_from_icon_name ("help-about", GTK_ICON_SIZE_LARGE_TOOLBAR);
    toolbutton = gtk_tool_button_new(icon, _("Help"));
    gtk_tool_item_set_tooltip_text(GTK_TOOL_ITEM(toolbutton), _("About"));
    g_signal_connect(G_OBJECT(toolbutton), "clicked",
                G_CALLBACK(gui_about_window), NULL);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), toolbutton, -1);

    icon = gtk_image_new_from_icon_name ("application-exit", GTK_ICON_SIZE_LARGE_TOOLBAR);
    toolbutton = gtk_tool_button_new(icon, _("Quit"));
    gtk_tool_item_set_tooltip_text(GTK_TOOL_ITEM(toolbutton), _("Quit"));
    g_signal_connect(G_OBJECT(toolbutton), "clicked",
                G_CALLBACK(gtk_main_quit),
                NULL);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), toolbutton, -1);

    gtk_widget_show_all(toolbar);
    gtk_widget_hide(GTK_WIDGET(refresh));

    return;
}

/* void gui_popup_create (void)
{
    static int have_selection = FALSE;
    GtkWidget *item;

    popup = gtk_menu_new();

    selection_widget = gtk_invisible_new();
    item = widget_menu_item_add(popup, _("Copy"));
    g_signal_connect(G_OBJECT(item), "activate",
                G_CALLBACK(gui_menu_copy_clicked),
                &have_selection);
    g_signal_connect(G_OBJECT(selection_widget), "selection_clear_event",
                G_CALLBACK(gui_menu_copy_clear),
                &have_selection);
    gtk_selection_add_target(selection_widget,
                    GDK_SELECTION_PRIMARY,
                    GDK_SELECTION_TYPE_STRING,
                    1);
    g_signal_connect(G_OBJECT(selection_widget), "selection_get",
                G_CALLBACK(gui_menu_copy_handle),
                &have_selection);

    return;
} */
