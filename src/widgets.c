/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#include "widgets.h"
#include "main.h"
#include "pixmap.h"
#include "options.h"
#include "utils.h"

#if GTK_CHECK_VERSION(3, 0, 0)
# define GTK_ADJ GtkAdjustment
#else
# define GTK_ADJ GtkObject
#endif

/* Buttons */
GtkWidget *
widget_button_color_add (GtkWidget *box, GCallback callback, const gint selector_idx, gdouble *color, const gchar *title)
{
    GdkPixbuf *pixbuf = NULL;
    GtkWidget *button;
    GtkWidget *hbox;
    GtkWidget *label;
    GtkWidget *image;

    GTK_BOX_V(hbox, TRUE, 1)
    gtk_container_add(GTK_CONTAINER(box), hbox);
    gtk_widget_show(hbox);

    label = gtk_label_new(title);
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show(label);

    button = gtk_button_new();
    gtk_box_pack_start(GTK_BOX(box), button, FALSE, FALSE, 0);
    gtk_widget_show(button);


    fill_pixbuf_color(color, &pixbuf, PREVIEW_W, PREVIEW_H);
    image = gtk_image_new_from_pixbuf(pixbuf);

    gtk_button_set_image(GTK_BUTTON(button), image);

    g_object_set_data(G_OBJECT(button), "pixmap", image);
    g_object_set_data(G_OBJECT(button), "color", color);

    g_signal_connect(G_OBJECT(button), "clicked", callback,
                GINT_TO_POINTER(selector_idx));

    return button;
}

/* Menus */
GtkWidget *
widget_menu_add (GtkWidget *item)
{
    GtkWidget *menu;

    menu = gtk_menu_new();
    if (item != NULL)
        gtk_menu_item_set_submenu(GTK_MENU_ITEM(item), menu);
    gtk_widget_show(menu);

    return menu;
}

GtkWidget *
widget_menu_item_add (GtkWidget *menu, const gchar *title)
{
    GtkWidget *item;

    if (title != NULL)
        item = gtk_menu_item_new_with_label(title);
    else {
        item = gtk_menu_item_new();
        gtk_widget_set_sensitive(item, FALSE);
    }
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
    gtk_widget_show(item);

    return item;
}

/* Frames */

GtkWidget *
widget_frame_add (GtkWidget *box, const gchar *title)
{
    GtkWidget *frame;
    GtkWidget *vbox;

    frame = gtk_frame_new(title);
    gtk_box_pack_start(GTK_BOX(box), frame, FALSE, FALSE, 3);
    gtk_widget_show(frame);

    GTK_BOX_V(vbox, FALSE, 1)
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 12);
    gtk_container_add(GTK_CONTAINER(frame), vbox);
    gtk_widget_show(vbox);

    return vbox;
}

/* Checks */
void
widget_check_clicked (GtkWidget *button, gint *value)
{
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button)))
        *value = 1;
    else
        *value = 0;

    return;
}

GtkWidget *
widget_check_add (GtkWidget *box, gint *value,
            const gchar *title)
{
    GtkWidget *check;

    check = gtk_check_button_new_with_label(title);
    if (*value)
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), TRUE);
    gtk_box_pack_start(GTK_BOX(box), check, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(check), "toggled",
                G_CALLBACK(widget_check_clicked),
                (gpointer)value);
    gtk_widget_show(check);

    return check;
}

/* Spins */

void
widget_spin_float_changed (GtkSpinButton *spin)
{
    gfloat *val;

    val = (gfloat *)g_object_get_data(G_OBJECT(spin), "value");
    *val = gtk_spin_button_get_value(spin);

    return;
}

GtkWidget *
widget_spin_add_float (GtkWidget *box, gfloat *value,
                gfloat min, gfloat max, gfloat step,
                const gchar *title)
{
    GTK_ADJ *adj;
    GtkWidget *hbox;
    GtkWidget *spin;
    GtkWidget *label;

    adj = gtk_adjustment_new(*value, min, max, step, step, 0);

    GTK_BOX_H(hbox, FALSE, 1)
    gtk_box_pack_start(GTK_BOX(box), hbox, FALSE, FALSE, 0);

    label = gtk_label_new(title);
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

    spin = gtk_spin_button_new(GTK_ADJUSTMENT(adj), 0.2, 3);
    gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(spin), TRUE);
    g_object_set_data(G_OBJECT(spin), "value", value);
    gtk_box_pack_end(GTK_BOX(hbox), spin, FALSE, FALSE, 0);
    g_signal_connect_swapped(G_OBJECT(adj), "value_changed",
                    G_CALLBACK(widget_spin_float_changed),
                    G_OBJECT(spin));

    gtk_widget_show_all(hbox);

    return spin;
}

void
widget_spin_int_changed (GtkSpinButton *spin)
{
    gint *val;

    val = (gint *)g_object_get_data(G_OBJECT(spin), "value");
    *val = gtk_spin_button_get_value(spin);

    return;
}

GtkWidget *
widget_spin_add_int (GtkWidget *box, gint *value,
                gint min, gint max, gint step,
                const gchar *title)
{
    GTK_ADJ *adj;
    GtkWidget *hbox;
    GtkWidget *spin;
    GtkWidget *label;

    adj = gtk_adjustment_new((gfloat)*value, (gfloat)min, (gfloat)max,
                    (gfloat)step, (gfloat)step, 0);

    GTK_BOX_H(hbox, FALSE, 1)
    gtk_box_pack_start(GTK_BOX(box), hbox, FALSE, FALSE, 0);

    label = gtk_label_new(title);
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

    spin = gtk_spin_button_new(GTK_ADJUSTMENT(adj), 0.2, 3);
    gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(spin), TRUE);
    g_object_set_data(G_OBJECT(spin), "value", value);
    gtk_box_pack_end(GTK_BOX(hbox), spin, FALSE, FALSE, 0);
    g_signal_connect_swapped(G_OBJECT(adj), "value_changed",
                    G_CALLBACK(widget_spin_int_changed),
                    G_OBJECT(spin));

    gtk_widget_show_all(hbox);

    return spin;
}

/* Radios */

void
widget_radio_clicked (GtkWidget *button, gint val)
{
    gint *value;

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button))) {
        value = (gint *) g_object_get_data(G_OBJECT(button), "value");
        *value = val;
    }

    return;
}

GtkWidget *
widget_radio_add (GtkWidget *box, GtkWidget *from,
            gint val, gint *value,
            const gchar *title)
{
    GtkWidget *radio;

    radio = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(from),
                                    title);
    gtk_box_pack_start(GTK_BOX(box), radio, TRUE, TRUE ,0);
    if (*value == val)
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio), TRUE);
    g_object_set_data(G_OBJECT(radio), "value", value);

    g_signal_connect(G_OBJECT(radio), "toggled",
                G_CALLBACK(widget_radio_clicked),
                GINT_TO_POINTER(val));
    gtk_widget_show(radio);

    return radio;
}

/* Entrys */

GtkWidget *
widget_entry_add_chars (GtkWidget *box, gchar **value, const gchar *title)
{
    GtkWidget *entry;
    GtkWidget *hbox;
    GtkWidget *label;

    GTK_BOX_H(hbox, FALSE, 1)
    gtk_box_pack_start(GTK_BOX(box), hbox, FALSE, FALSE, 8);

    label = gtk_label_new(title);
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

    entry = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(entry), *value);
    gtk_entry_set_width_chars(GTK_ENTRY(entry), 100);
    g_object_set_data(G_OBJECT(entry), "value", value);
    gtk_box_pack_end(GTK_BOX(hbox), entry, FALSE, FALSE, 0);

    gtk_widget_show_all(hbox);

    return entry;
}

void
widget_entry_get_list (GtkWidget *entry, GList **glist, const gchar *del)
{
    const gchar *tmp = gtk_entry_get_text(GTK_ENTRY(entry));
    g_list_free_full(*glist, g_free);
    *glist = NULL;
    *glist = strutils_strdelim_to_glist(tmp, del);

    return;
}

void
widget_entry_get_chars (GtkWidget *entry, gchar *text[])
{
    if (*text != NULL)
        g_free(*text);
    *text = g_strdup(gtk_entry_get_text(GTK_ENTRY(entry)));

    return;
}

/* All */

void
widget_set_sensitivity_invert (GtkWidget *widget)
{
    if (gtk_widget_get_sensitive(widget))
        gtk_widget_set_sensitive(widget, FALSE);
    else
        gtk_widget_set_sensitive(widget, TRUE);

    return;
}
