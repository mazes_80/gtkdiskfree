/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#ifndef GTKDISKFREE_MENUS_H
#define GTKDISKFREE_MENUS_H

#include <gtk/gtk.h>
#include "main.h"

gboolean gui_popup_menu_signal (GtkWidget *widget, GdkEventButton *ev, gpointer data);

void gui_main_menu_create (GtkWidget *box);
void gui_toolbar_create (GtkWidget *box);
void gui_popup_create (void);
#endif // GTKDISKFREE_MENUS_H
