/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#include "dialog.h"
#include "main.h"
#include "pixmap.h"
#include "configure.h"

extern GtkWidget *mwindow;

GdkPixbuf *logo;

void
gui_about_window ()
{
    static GtkWidget *about_dialog = NULL;

    if(about_dialog==NULL) {
        const gchar *authors[] = {
                                _("main: Dj-Death (Landwerlin Lionel) "),
                                _("maintainer: mazes_80 (Bauer Samuel) <samuel.bauer@yahoo.fr>"),
                                _("contributor: Vyacheslav Dikonov <sdiconov@mail.ru>"),
                                _("contributor: Dave Lampe <djl@dplace.com>"),
                                NULL
        };
        const gchar *translators = _("French : Dj-Death (Lionel Landwerlin) \n"
                                    "French : mazes_80 (Bauer Samuel) <samuel.bauer@yahoo.fr>\n"
                                    "German : Holger Wansing <linux@wansing-online.de>\n"
                                    "Polish : Zbigniew M. Kempczyn'ski <sdiconov@mail.ru>\n"
                                    "Russian : Vyacheslav Dikonov <djl@dplace.com>\n");
        const gchar *license = {
            _("  GtkDiskFree is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by Free Software Foundation; either version2 of the License, or (at your options) any later version.\n\n  GtkDiskFree is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n\n  You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.")
        };

        about_dialog = gtk_about_dialog_new();
        gtk_widget_realize( about_dialog );

        gtk_window_set_transient_for( GTK_WINDOW( about_dialog ), GTK_WINDOW( mwindow ) );
        gtk_about_dialog_set_program_name( GTK_ABOUT_DIALOG(about_dialog), PACKAGE_NAME );
        gtk_about_dialog_set_version( GTK_ABOUT_DIALOG(about_dialog), PACKAGE_VERSION );
        gtk_about_dialog_set_copyright( GTK_ABOUT_DIALOG(about_dialog),
                                        _("Copyright 2001 © Landwerlin Lionel"
                                            "\n          2012 © Bauer Samuel") );
        gtk_about_dialog_set_website( GTK_ABOUT_DIALOG(about_dialog), "https://gitlab.com/mazes_80/gtkdiskfree" );
        gtk_about_dialog_set_authors( GTK_ABOUT_DIALOG(about_dialog), authors );
        gtk_about_dialog_set_license( GTK_ABOUT_DIALOG(about_dialog), license );
        gtk_about_dialog_set_wrap_license( GTK_ABOUT_DIALOG(about_dialog), TRUE );
        gtk_about_dialog_set_translator_credits( GTK_ABOUT_DIALOG(about_dialog), translators );
        logo = pixmap_draw_fs_snail_chart(about_dialog, 100, 100, 50, 0);
        gtk_about_dialog_set_comments( GTK_ABOUT_DIALOG(about_dialog), _("GTK+ implementation of the GNU df shell command."));

    }

    gtk_about_dialog_set_logo( GTK_ABOUT_DIALOG(about_dialog), logo );

    gtk_dialog_run( GTK_DIALOG( about_dialog ) );
    gtk_widget_hide( about_dialog );
}

void error_dialog (const gchar *text)
{
    gchar *lbl;
    GtkWidget *window;
    GtkWidget *hbox;
    GtkWidget *label;
    GtkWidget *button;
    GtkWidget *pixmap;

    window = gtk_dialog_new();
    gtk_window_set_transient_for(GTK_WINDOW(window), GTK_WINDOW(mwindow));
    gtk_window_set_resizable(GTK_WINDOW(window), FALSE);
    gtk_window_set_title(GTK_WINDOW(window), _("gtkdiskfree : error"));
    gtk_window_set_modal(GTK_WINDOW(window), TRUE);
    gtk_container_set_border_width(GTK_CONTAINER(window), 8);

    GTK_BOX_H(hbox, FALSE, 1)
    gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(window))),
                hbox, FALSE, FALSE, 0);

    pixmap = gtk_image_new_from_icon_name ("dialog-error", GTK_ICON_SIZE_LARGE_TOOLBAR);
    gtk_box_pack_start(GTK_BOX(hbox), pixmap, FALSE, FALSE, 0);


    lbl = g_strdup_printf("error : %s", text);
    label = gtk_label_new(lbl);
    g_free(lbl);
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

    button = gtk_dialog_add_button(GTK_DIALOG(window), _("OK"), GTK_RESPONSE_OK);
    gtk_widget_grab_default(button);

    gtk_widget_show_all(window);
    gtk_dialog_run(GTK_DIALOG(window));
    gtk_widget_destroy(window);

    return;
}

void widget_dialog_chart(GtkTreeModel *model, GtkTreeIter *iter) {
    GtkWidget *fs_view;
    GtkWidget *box;
    GtkWidget *image;
    GtkWidget *label;
    GdkPixbuf *pixbuf;
    gchar *dev, *mntpt;
    gulong free_space, size;

    gtk_tree_model_get (model, iter,
                        FILESYSTEM_COLUMN, &dev,
                        SIZEK_COLUMN, &size,
                        FREEK_SPACE_COLUMN, &free_space,
                        MOUNT_POINT_COLUMN, &mntpt,
                        -1);

    fs_view = gtk_dialog_new();
    gtk_window_set_transient_for(GTK_WINDOW(fs_view), GTK_WINDOW(mwindow));
    gtk_window_set_resizable(GTK_WINDOW(fs_view), FALSE);
    gtk_window_set_title(GTK_WINDOW(fs_view), _("Filesystem Chart"));
    gtk_container_set_border_width(GTK_CONTAINER(fs_view), 8);
    gtk_widget_show(fs_view);

    GTK_BOX_V(box, FALSE, 1)
    gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(fs_view))),
                box, FALSE, FALSE, 0);

    label = gtk_label_new(mntpt);
    g_free(mntpt);
    gtk_box_pack_end(GTK_BOX(box), label, FALSE, FALSE, 0);

    label = gtk_label_new(dev);
    g_free(dev);
    gtk_box_pack_end(GTK_BOX(box), label, FALSE, FALSE, 0);

    /* select renderer */
    if(options->popup[0] == 1)
        pixbuf = pixmap_draw_fs_pie_chart  (mwindow, 150, size, free_space, 0);
    else {
        pixbuf = pixmap_draw_fs_snail_chart(mwindow, 150, size, free_space, 0);

        image = gtk_image_new_from_pixbuf(pixbuf);
        gtk_box_pack_start(GTK_BOX(box), image, FALSE, FALSE, 0);

        pixbuf = pixmap_draw_legend(mwindow, 150, 16);
    }

    image = gtk_image_new_from_pixbuf(pixbuf);
    gtk_box_pack_start(GTK_BOX(box), image, FALSE, FALSE, 0);

    gtk_widget_show_all(fs_view);
    gtk_dialog_run(GTK_DIALOG(fs_view));
    gtk_widget_destroy(fs_view);
}

