/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#ifndef GTKDISKFREE_INTERFACE_H
#define GTKDISKFREE_INTERFACE_H
#include <gtk/gtk.h>
#include "main.h"
extern GtkWidget *mwindow;

extern GtkTreeModel *list_treemodel;
extern GtkWidget *list_treeview;

extern GtkWidget *menu_frame;
extern GtkWidget *tool_frame;

extern gboolean gui_color_update;

void gui_list_columns_disp_update (void);
void gui_statusbar_timeout_update (GtkWidget *widget, order_t action);
gboolean gui_statusbar_update_block (void);
void gui_main_window_disp_update (void);
void gui_main_window_resize (void);
void gui_main_window_create (void);
void gui_list_capacities_init(GList *list);
gboolean gui_list_capacities_redraw ();
#endif // GTKDISKFREE_INTERFACE_H
