#ifndef GTKDISKFREE_MAIN_H
#define GTKDISKFREE_MAIN_H
#ifdef HAVE_CONFIG_H            /* Config */
# include "config.h"
#endif

#include <glib/gi18n-lib.h>

// Columns
enum {
    FILESYSTEM_COLUMN,
    SIZE_COLUMN,
    USED_SPACE_COLUMN,
    FREE_SPACE_COLUMN,
    USED_PERCENT_COLUMN,
    FREE_PERCENT_COLUMN,
    FILESYSTEM_TYPE_COLUMN,
    MOUNT_OPTIONS_COLUMN,
    MOUNT_POINT_COLUMN,
    MOUNT_COLUMN,
    CAPACITY_COLUMN,

    SIZEK_COLUMN,
    USEDK_SPACE_COLUMN,
    FREEK_SPACE_COLUMN,
    USEDP_PERCENT_COLUMN,
    FREEP_PERCENT_COLUMN,
    STATUS_COLUMN,
    N_COLUMNS,
};

// Orders
typedef enum {
    DEFAULT,
    READ,
    WRITE,

    REFRESH,
    STOP,

    ADD,
    INSERT,
    UPDATE,
    REMOVE,
    MOUNT,
    UMOUNT
} order_t;

// Status
enum {
    FS_OLD,
    FS_MOUNTED,
    FS_UMOUNTED
};

enum {
    KILOBYTE,
    MEGABYTE,
    GIGABYTE,
    AUTOSIZE
};

#ifdef DEBUG
# define PRINT_DEBUG(args) (printf("** gtkdiskfree debug: "), printf args, printf(" **\n"))
#else
# define PRINT_DEBUG(args)
#endif

#endif // GTKDISKFREE_MAIN_H
