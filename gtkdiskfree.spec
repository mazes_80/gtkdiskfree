%define release 1
%define prefix  /usr
%define name	gtkdiskfree
%define version 2.0.4
%define _menudir %{prefix}/share/applications

Summary:	GtkDiskFree shows free space on the mounted filesystems.
Summary(ru_RU.CP1251):	GtkDiskFree - èíäèêàòîð ñâîáîäíîãî ìåñòà íà äîñòóïíûõ äèñêàõ è ðàçäåëàõ.
Summary(ru_RU.KOI8-R): GtkDiskFree - ÉÎÄÉËÁÔÏÒ Ó×ÏÂÏÄÎÏÇÏ ÍÅÓÔÁ ÎÁ ÄÏÓÔÕÐÎÙÈ ÄÉÓËÁÈ É ÒÁÚÄÅÌÁÈ.
Name:		%{name}
License:	GPL
Version:    	%{version}
Release:	%{release}
Vendor:		Dj-Death (Landwerlin Lionel) <djdeath@gmx.fr>
Url:		http://gtkdiskfree.tuxfamily.org
Group:		Utilities/System
Source:		%{name}-%{version}.tar.gz
BuildRoot:	/tmp/gtkdiskfree-%{version}-root
Docdir:         %{prefix}/doc

%description
GtkDiskFree is a program which shows free space on your mounted filesystems.
It shows the filesystem type too and other information Now Gtkdiskfree can 
mount and unmount different filesystems. GtkDiskFree is written with the GTK+ 
library (www.gtk.org)

%description -l ru_RU.CP1251
Ïðîãðàììà GtkDiskFree ïðåäíàçíà÷åíà äëÿ îòîáðàæåíèÿ êîëè÷åñòâà ñâîáîäíîãî 
ìåñòà èìåþùåãîñÿ íà äîñòóïíûõ äèñêàõ è ðàçäåëàõ. Êðîìå ýòîãî, GtkDiskFree
ïîêàçûâàåò òèï ôàéëîâîé ñèñòåìû è äðóãóþ ñëóæåáíóþ èíôîðìàöèþ, à òàêæå ìîæåò 
ìîíòèðîâàòü ðàçäåëû. GtkDiskFree èñïîëüçóåò áèáëèîòåêó GTK+ (www.gtk.org).

%description -l ru_RU.KOI8-R
ðÒÏÇÒÁÍÍÁ GtkDiskFree ÐÒÅÄÎÁÚÎÁÞÅÎÁ ÄÌÑ ÏÔÏÂÒÁÖÅÎÉÑ ËÏÌÉÞÅÓÔ×Á Ó×ÏÂÏÄÎÏÇÏ 
ÍÅÓÔÁ ÉÍÅÀÝÅÇÏÓÑ ÎÁ ÄÏÓÔÕÐÎÙÈ ÄÉÓËÁÈ É ÒÁÚÄÅÌÁÈ. ëÒÏÍÅ ÜÔÏÇÏ, GtkDiskFree
ÐÏËÁÚÙ×ÁÅÔ ÔÉÐ ÆÁÊÌÏ×ÏÊ ÓÉÓÔÅÍÙ É ÄÒÕÇÕÀ ÓÌÕÖÅÂÎÕÀ ÉÎÆÏÒÍÁÃÉÀ, Á ÔÁËÖÅ ÍÏÖÅÔ 
ÍÏÎÔÉÒÏ×ÁÔØ ÒÁÚÄÅÌÙ. GtkDiskFree ÉÓÐÏÌØÚÕÅÔ ÂÉÂÌÉÏÔÅËÕ GTK+ (www.gtk.org).

%prep
%setup 

%build
./configure --prefix=%{prefix}
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{prefix}/bin
mkdir -p %{buildroot}%{prefix}/share/pixmaps
cp ./gtkdiskfree.png %{buildroot}%{prefix}/share/pixmaps/gtkdiskfree.png
mkdir -p %{buildroot}%{prefix}/share/applications
cp ./gtkdiskfree.desktop %{buildroot}%{prefix}/share/applications/gtkdiskfree.desktop
make install prefix=%{buildroot}%{prefix}
cp ./src/gtkdiskfree %{buildroot}%{prefix}/bin/

mkdir -p $RPM_BUILD_ROOT/%{_menudir}
cat << EOF > $RPM_BUILD_ROOT/%{_menudir}/%{name}
?package(%{name}): \
	needs="X11" \
	section="Applications/" \
	title="GtkDiskFree" \
	longtitle="GtkDiskFree shows free space on the mounted filesystems" \
	command="%{_bindir}/gtkdiskfree" \
	icon=gtkdiskfree.png
EOF



%files
%attr(-, root, root)
%{prefix}/bin/gtkdiskfree
%{prefix}/share/locale/*/LC_MESSAGES/gtkdiskfree.mo
%{prefix}/share/applications/gtkdiskfree.desktop
%{prefix}/share/pixmaps/gtkdiskfree.png
%doc AUTHORS NEWS README COPYING
/%{_menudir}/%{name}

%clean
rm -rf %{buildroot}
